# quitter

The quitter.sh script allows you to schedule appointments over the course of a day and warns when you need to attend.
Requires the **xmessage** program.

_Le script quitter.sh vous permet de planifier des rendez-vous sur une journée et prévient lorsque vous devez y assister.
Nécessite le programme **xmessage**._

______________________________________

# How to use it ? | Comment l'utiliser ?

As soon as an appointment is added, a **quitter** directory is automatically created in ~/.config .
This directory contains the file **horaires.db** (stores appointments) and **boucle.pid** (PID of loop comparing actual date to dates in horaires.db).

_Dès lors qu'on ajoute un rendez-vous, il est créé automatiquement un répertoire **quitter** situé dans ~/.config .
Ce répertoire contient le fichier **horaires.db** (stocke les rendez-vous) ainsi que **boucle.pid** (PID of loop comparing actual date to dates in horaires.db)_

Obviously, it is quite possible to place the quit.sh script in your /usr/bin to consider the script as a separate command.

_Évidemment, il est tout à fait possible de placer le script quitter.sh dans votre /usr/bin pour considérer le script comme une commande à part entière._

```
Usage: quitter {
        HHMM message... [+tag ...]
        -q
        -l [+tag ...]
        -a [+tag ...]
        -r HHMM || -r +tag
        -h }"
```
- -q : stops loop which compare actual date to date in the file horaires.db.
- -l : list of appointments to come. Possibility to list thanks to tags.
- -a : list all appointments. Possibility to list thanks to tags.
- -r : remove appointments in function of date (in this form : HHMM) or in function of tags.
- -h : help (**without** arguments/parameters -> help)

# Examples : 

```
./quitter.sh 1450 I lunch with Juliet 
./quitter.sh 1622 Playing football +friends +ball +stadium
```


- If quitter.sh is in /usr/bin _(and rename in quitter)_, then :

```
quitter 1225 I lunch with Charlotte
quitter 2000 Watch TV +family +sofa
```

