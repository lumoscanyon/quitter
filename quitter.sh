#!/bin/bash
#
# VIRGOS Antoine, LAU Yoann S1B''
#
# Copyright © 2020, Virgos & Lau Company
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), 
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, 
# fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders X be liable for any claim, damages or other liability, 
# whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
#
# Except as contained in this notice, the name of the Virgos & Lau Company shall not be used in advertising or otherwise to promote the sale, use or other dealings 
# in this Software without prior written authorization from the Virgos & Lau Company.
######################################################################

######################################################################
# Template
######################################################################
set -o errexit # Exit if command failed.
set -o pipefail # Exit if pipe failed.
set -o nounset # Exit if variable not set.
# Remove the initial space and instead use '\n'.
IFS=$'\n\t'

######################################################################
# Global variables
######################################################################
FOLDERNAME="${HOME}"/.config/quitter
FILENAME="${FOLDERNAME}"/horaires.db
PID_FILE="${FOLDERNAME}"/boucle.pid
PROG=/usr/bin/xmessage

ARGUMENTS="$(echo "${@}")"
ARGUMENTS_NO_TAG="$(echo "${@}" | cut -d '+' -f 1 )"
ARGUMENTS_TAG="$(echo "${@}" | sed 's/^[^+]*+/+/')"
ARGUMENT_DATE="$(echo "${@}" | cut -d ' ' -f 1)"
ARGUMENT_REMOVE="$(echo "${@}" | cut -d ' ' -f 2)"

DATE_COURANTE="$(date +%H%M)"
DATE_ARGUMENT_HEURE="$(echo "${@}" | cut -c 1-2)"
DATE_ARGUMENT_MINUTES="$(echo "${@}" | cut -c 3-4)"

######################################################################
# Créer le fichier horaires.db situé dans le répertoire ~/.config/quitter
# et le fichier boucle.pid aussi dans ~/.config/quitter
#
# Globals:
#   FOLDERNAME
#   FILENAME
#   PID_FILE
# Locals:
#   None
# Arguments:
#   None
# Return:
#   0 si les fichier se sont crééés dans le répertoire, non-0
#   si erreur.  
######################################################################
function create_file() {
    if test ! -f "${FILENAME}"
    then 
        mkdir -p "${FOLDERNAME}" && touch "${FILENAME}"
    fi

    if test ! -f "${PID_FILE}"
    then
        touch "${PID_FILE}"
    fi
}

######################################################################
# Ajoute un événement dans le fichier horaires.db
# 
# Globals:
#   FILENAME
#   ARGUMENTS
#   ARGUMENT_DATE
#   DATE_COURANTE
#   DATE_ARGUMENT_HEURE
#   DATE_ARGUMENT_MINUTES
# Locals:
#   checkPremiereLigne
#   compteLignesFile
# Arguments:
#   HHMM message... [+tag ...]
#   (l'argument de tag est optionnel)
# Return:
#   Si l'on ajoute un événément valide et strictement supérieur à l'horaire actuel
#   alors on affiche, sur la sortie standard, l'événement et l'on redirige
#   cela dans le fichier horaires.db ; sans écraser. Sinon, on affiche,
#   sur la sortie standard, un rappel pour saisir un horaire.
######################################################################
function add_event() {
    if test "${ARGUMENT_DATE}" -gt "${DATE_COURANTE}" && test ! "${DATE_ARGUMENT_HEURE}" -ge 24 && test ! "${DATE_ARGUMENT_MINUTES}" -ge 60
    then
        create_file
        echo "${ARGUMENTS}" >> "${FILENAME}"
        sort -o "${FILENAME}" "${FILENAME}"

        local checkPremiereLigne="$(cat "${FILENAME}" | grep '[0-9][0-9][0-9][0-9]' | cut -d ' ' -f 1  | sed -n '1p')"
        local compteLignesFile="$(cat "${FILENAME}" | wc -l)"

        if test "${checkPremiereLigne}" = "${ARGUMENT_DATE}" && test "${compteLignesFile}" -eq 1
        then
            compare 
        fi
    else 
        echo "Merci de saisir un horaire valide et strictement supérieur à l'horaire actuel. 
    Forme : HHMM."
    fi
}

######################################################################
# Compare l'heure du programme GNU date avec celles des événements du 
# fichier horaires.db et affiche le message des événements (sans les tags)
#  
# Globals:
#   FILENAME
#   PROG
#   PID_FILE
#   $!
# Locals:
#   dateCourante
#   dateFuturEvenement
#   affichageLignes
# Arguments:
#   None
# Return:
#   0 si la boucle de comparaison fonctionne, non-0 si erreur.
######################################################################
function compare() {

    while true
    do
        local dateCourante="$(date +%H%M)"  
        local dateFuturEvenement="$(cat "${FILENAME}" | cut -d ' ' -f 1 | grep "${dateCourante}" | head -n 1)"
        local affichageLignes="$(cat "${FILENAME}" | cut -d '+' -f 1 | grep "${dateCourante}")"

        if test "${dateCourante}" = "${dateFuturEvenement}"
        then
            "${PROG}" "${affichageLignes}" -buttons Ok -geometry 400x100 -center &
        fi
        sleep 30
    done &

    echo "${!}" > "${PID_FILE}"
}

######################################################################
# Arrête la boucle de la fonction compare grâce au PID récupéré
#  
# Globals:
#   PID_FILE
# Locals:
#   contenu_pid_file
# Arguments:
#   -q
# Return:
#   0 si la boucle compare s'arrête, non-0 si erreur.
######################################################################
function destroy_loop {
    local contenu_pid_file="$(cat "${PID_FILE}")"

    kill -9 "${contenu_pid_file}"
    rm "${PID_FILE}"
}

######################################################################
# Par défaut, afficher la liste des tous les événéments à venir.
# Possibilité d'afficher uniquement les événements à venir
# correspondant à un ou plusieurs tags particuliers
#  
# Globals:
#   FILENAME
#   DATE_COURANTE
#   ARGUMENTS_TAG
# Locals:
#   dateFile
#   premier_caractere
#   tags_file
#   checkLigneTag
# Arguments:
#   -l [+tag ...]
# Outputs:
#   Affiche, sur la sortie standard, les événements futurs ou bien
#   affiche les événements futurs selon un ou plusieurs tags donnés.
######################################################################
function list_future_event {   
    while read line
    do      
        local dateFile="$(cat "${FILENAME}" | echo "${line}" | cut -d ' ' -f 1 | head -n 1)"

        if test "${dateFile}" -gt "${DATE_COURANTE}" && test ${ARGUMENTS_TAG} = "-l"
        then
            echo "${line}" | cut -d '+' -f 1 > lignes_futures.txt
            cat lignes_futures.txt
            rm lignes_futures.txt
        fi

        local premier_caractere="$(echo "${ARGUMENTS_TAG}" | head -c 1)"

        if test "${dateFile}" -gt "${DATE_COURANTE}" && test "${premier_caractere}" = "+"
        then
            local tags_file="$(cat "${FILENAME}" | grep '+' | sed 's/^[^+]*+/+/' | echo "${ARGUMENTS_TAG}" | grep "${ARGUMENTS_TAG}" | tr " " "\n")"

            echo "${line}" > lignes_futures.txt

            local checkLigneTag="$(cat lignes_futures.txt | grep "${ARGUMENTS_TAG}")"

            echo "${checkLigneTag}" | cut -d '+' -f 1 | sed '/^$/d' > lignes_futures.txt
            cat lignes_futures.txt
            rm lignes_futures.txt      
        fi
    done < "${FILENAME}"
}

######################################################################
# Retirer un ou plusieurs événement(s) selon un horaire ou un tag  
# 
# Globals:
#   FILENAME
#   ARGUMENT_REMOVE
# Locals:
#   None
# Arguments:
#   -r HHMM || -r +tag
# Outputs:
#   Affiche, sur la sortie standard, tous les événements qui ne
#   correspondent pas à l'horaire ou au tag donné. Le résultat est
#   ensuite redirigé dans le fichier horaires.db.
######################################################################
function remove_event {
    sed -i "/"${ARGUMENT_REMOVE}"\b/d" "${FILENAME}"
}

######################################################################
# Par défaut, affiche la liste de tous les événements.
# Possibilité d'afficher uniquement les événements correspondant à un ou plusieurs
# tags particuliers
#  
# Globals:
#   FILENAME
#   ARGUMENTS_TAG
# Locals:
#   premier_caractere
#   tags_file
#   lignes_tag
# Arguments:
#   -a [+tag ...]
# Outputs:
#   Affiche, sur la sortie standard, les évenements correspondant 
#   au(x) tag(s) sinon affiche tous les événements sans distinction.
######################################################################
function list_all_event {
    local premier_caractere="$(echo "${ARGUMENTS_TAG}" | head -c 1)"

    if test "${premier_caractere}" = "+"
    then
        local tags_file="$(cat "${FILENAME}" | grep '+' | sed 's/^[^+]*+/+/' | echo "${ARGUMENTS_TAG}" | grep "${ARGUMENTS_TAG}" | tr " " "\n")"
        local lignes_tag="$(cat "${FILENAME}" | grep "${tags_file}" > lignes_tag.txt)"

        cat lignes_tag.txt
        rm lignes_tag.txt
    else
        cat "${FILENAME}"
    fi
}

######################################################################
# Obtenir de l'aide
#
# Globals:
#   None
# Locals:
#   None
# Arguments:
#   -h
# Outputs:
#   Affiche, sur la sortie standard, comment utiliser le script.
######################################################################
function help() {
    echo "Usage: quitter {
        HHMM message... [+tag ...]
        -q
        -l [+tag ...]
        -a [+tag ...]
        -r HHMM || -r +tag
        -h }"
}

######################################################################
# Main
######################################################################
if test "${#}" = 0
then
    help
else
    case "${1}" in
    [0-9][0-9][0-9][0-9])
        add_event ;;
    -q)
        destroy_loop ;;
    -l)
        list_future_event ;;
    -r)
        remove_event ;; 
    -a)
        list_all_event ;;    
    -h)
        help ;;
    *)
        help ;;
    esac
fi 

        